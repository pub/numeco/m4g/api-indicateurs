package org.mte.numecoeval.indicateurs.infrastructure.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactApplicatif;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactEquipementPhysique;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactEquipementVirtuel;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactMessagerie;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactReseau;
import org.mte.numecoeval.indicateurs.domain.port.output.IndicateurPersistencePort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class IndicateurController {
    private IndicateurPersistencePort<IndicateurImpactReseau> indicateurImpactReseauJpaAdapter;
    private IndicateurPersistencePort<IndicateurImpactEquipementPhysique> indicateurImpactEquipementJpaAdapter;
    private IndicateurPersistencePort<IndicateurImpactEquipementVirtuel> indicateurImpactEquipementVirtuelJpaAdapter;
    private IndicateurPersistencePort<IndicateurImpactApplicatif> indicateurImpactApplicatifJpaAdapter;
    private IndicateurPersistencePort<IndicateurImpactMessagerie> indicateurImpactMessagerieJpaAdapter;


    @Operation(
            summary = "Supprimer les indicateurs d'impact des équipement physiques",
            tags = "Purge des indicateurs"
    )
    @DeleteMapping("/indicateur/indicateursEquipementPhysiques")
    public void deleteAllIndicateursEquipementsPhysiques(){
        indicateurImpactEquipementJpaAdapter.deleteAll();
    }
    @Operation(
            summary = "Supprimer les indicateurs d'impact des équipement virtuels",
            tags = "Purge des indicateurs"
    )
    @DeleteMapping("/indicateur/indicateursEquipementVirtuels")
    public void deleteAllIndicateursEquipementsVirtuels(){
        indicateurImpactEquipementVirtuelJpaAdapter.deleteAll();
    }
    @Operation(
            summary = "Supprimer les indicateurs d'impact réseau",
            tags = "Purge des indicateurs"
    )
    @DeleteMapping("/indicateur/indicateursImpactsReseaux")
    public void deleteAllIndicateursReseaux(){
        indicateurImpactReseauJpaAdapter.deleteAll();
    }
    @Operation(
            summary = "Supprimer les indicateurs d'impact des applications",
            tags = "Purge des indicateurs"
    )
    @DeleteMapping("/indicateur/indicateursImpactApplicatifs")
    public void deleteAllIndicateursApplicatifs(){
        indicateurImpactApplicatifJpaAdapter.deleteAll();
    }

    @Operation(
            summary = "Supprimer les indicateurs d'impact messagerie",
            tags = "Purge des indicateurs"
    )
    @DeleteMapping("/indicateur/indicateursImpactMessageries")
    public void deleteAllIndicateursMessagerie(){
        indicateurImpactMessagerieJpaAdapter.deleteAll();
    }
}
