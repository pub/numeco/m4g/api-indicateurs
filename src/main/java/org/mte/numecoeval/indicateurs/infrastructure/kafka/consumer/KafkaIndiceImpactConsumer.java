package org.mte.numecoeval.indicateurs.infrastructure.kafka.consumer;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactApplicatif;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactEquipementPhysique;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactEquipementVirtuel;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactMessagerie;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactReseau;
import org.mte.numecoeval.indicateurs.domain.port.output.IndicateurPersistencePort;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactApplicatifMapper;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactEquipementMapper;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactMessagerieMapper;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactReseauMapper;
import org.mte.numecoeval.topic.computeresult.MessageIndicateurs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class KafkaIndiceImpactConsumer {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaIndiceImpactConsumer.class);

    @NonNull
    private IndicateurImpactReseauMapper indicateurImpactReseauMapper;
    @NonNull
    private IndicateurImpactEquipementMapper indicateurImpactEquipementMapper;
    @NonNull
    private IndicateurImpactMessagerieMapper indicateurImpactMessagerieMapper;
    @NonNull
    private IndicateurImpactApplicatifMapper indicateurImpactApplicatifMapper;
    @NonNull
    private IndicateurPersistencePort<IndicateurImpactReseau> indicateurImpactReseauJpaAdapter;
    @NonNull
    private IndicateurPersistencePort<IndicateurImpactEquipementPhysique> indicateurImpactEquipementPhysiqueJpaAdapter;
    @NonNull
    private IndicateurPersistencePort<IndicateurImpactApplicatif> indicateurImpactApplicatifJpaAdapter;
    @NonNull
    private IndicateurPersistencePort<IndicateurImpactMessagerie> indicateurImpactMessagerieJpaAdapter;
    @NonNull
    private IndicateurPersistencePort<IndicateurImpactEquipementVirtuel> indicateurImpactEquipementVirtuelJpaAdapter;

    private MessageIndicateurs dataBuffer = null;

    private StopWatch stopWatch = StopWatch.create();

    @KafkaListener(topics = "${numecoeval.kafka.topic.indicateur.name}", containerFactory = "consumerIndicateurImpactReseauFactory")
    public void indicateurListener(MessageIndicateurs messageIndicateurs) {
        LOG.debug("Message reçu  : {}", messageIndicateurs);

        if(dataBuffer == null) {
            LOG.info("Début de réception des indicateurs");
            dataBuffer = new MessageIndicateurs();
            stopWatch.reset();
            stopWatch.start();
        }

        dataBuffer.getImpactsEquipementsPhysiques().addAll(messageIndicateurs.getImpactsEquipementsPhysiques());
        dataBuffer.getImpactsEquipementsVirtuels().addAll(messageIndicateurs.getImpactsEquipementsVirtuels());
        dataBuffer.getImpactApplications().addAll(messageIndicateurs.getImpactApplications());
        dataBuffer.getImpactsReseaux().addAll(messageIndicateurs.getImpactsReseaux());
        dataBuffer.getImpactMessageries().addAll(messageIndicateurs.getImpactMessageries());
    }



    @KafkaListener(topics = "${numecoeval.kafka.topic.indicateur.name}-flush", containerFactory = "consumerIndicateurImpactReseauFactory")
    public void indicateurFlushListener(MessageIndicateurs messageIndicateurs) {
        LOG.info("Message de fin de production d'indicateurs reçu : {}", messageIndicateurs);
        StopWatch stopWatchReseau = StopWatch.createStarted();
        indicateurImpactReseauJpaAdapter.saveAll(
                dataBuffer.getImpactsReseaux().stream()
                        .map(indicateurImpactReseauDTO -> indicateurImpactReseauMapper.toIndicateurImpactReseau(indicateurImpactReseauDTO))
                        .toList()
        );
        stopWatchReseau.stop();

        LOG.info("Fin du traitement des impacts réseaux {} en {} secondes",
                CollectionUtils.emptyIfNull(dataBuffer.getImpactsReseaux()).size(),
                stopWatchReseau.getTime(TimeUnit.SECONDS)
        );

        StopWatch stopWatchEquipementsPhysique = StopWatch.createStarted();
        indicateurImpactEquipementPhysiqueJpaAdapter.saveAll(
                dataBuffer.getImpactsEquipementsPhysiques().stream()
                        .map(indicateurImpactEquipementDTO -> indicateurImpactEquipementMapper.toIndicateurImpactEquipementPhysique(indicateurImpactEquipementDTO))
                        .toList()
        );
        LOG.info("Fin du traitement des impacts d'équipements physiques {} en {} secondes",
                CollectionUtils.emptyIfNull(dataBuffer.getImpactsEquipementsPhysiques()).size(),
                stopWatchEquipementsPhysique.getTime(TimeUnit.SECONDS)
        );

        StopWatch stopWatchEquipementsVirtuel = StopWatch.createStarted();
        indicateurImpactEquipementVirtuelJpaAdapter.saveAll(
                dataBuffer.getImpactsEquipementsVirtuels().stream()
                        .map(dto ->indicateurImpactEquipementMapper.toIndicateurImpactEquipementVirtuel(dto))
                        .toList()
        );
        LOG.info("Fin du traitement des impacts d'équipements virtuels {} en {} secondes",
                CollectionUtils.emptyIfNull(dataBuffer.getImpactsEquipementsVirtuels()).size(),
                stopWatchEquipementsVirtuel.getTime(TimeUnit.SECONDS)
        );

        StopWatch stopWatchApplications = StopWatch.createStarted();
        indicateurImpactApplicatifJpaAdapter.saveAll(
                dataBuffer.getImpactApplications().stream()
                        .map(indicateurImpactApplicationDTO -> indicateurImpactApplicatifMapper.toIndicateurImpactApplicatif(indicateurImpactApplicationDTO))
                        .toList()
        );
        LOG.info("Fin du traitement des impacts d'applications {} en {} secondes",
                CollectionUtils.emptyIfNull(dataBuffer.getImpactApplications()).size(),
                stopWatchApplications.getTime(TimeUnit.SECONDS)
        );

        StopWatch stopWatchMessageries = StopWatch.createStarted();
        indicateurImpactMessagerieJpaAdapter.saveAll(
                dataBuffer.getImpactMessageries().stream()
                        .map(indicateurImpactMessagerieDTO->indicateurImpactMessagerieMapper.toIndicateurImpactMessagerie(indicateurImpactMessagerieDTO))
                        .toList()
        );
        LOG.info("Fin du traitement des impacts de Messageries {} en {} secondes",
                CollectionUtils.emptyIfNull(dataBuffer.getImpactMessageries()).size(),
                stopWatchMessageries.getTime(TimeUnit.SECONDS)
        );

        stopWatch.stop();
        LOG.info("Fin du traitement des indicateurs : Nombre total d'indicateurs {} en {} secondes",
                CollectionUtils.emptyIfNull(dataBuffer.getImpactsReseaux()).size()
                        + CollectionUtils.emptyIfNull(dataBuffer.getImpactsEquipementsPhysiques()).size()
                        + CollectionUtils.emptyIfNull(dataBuffer.getImpactApplications()).size()
                        + CollectionUtils.emptyIfNull(dataBuffer.getImpactMessageries()).size(),
                stopWatch.getTime(TimeUnit.SECONDS)
        );
        dataBuffer = null;
    }
}
