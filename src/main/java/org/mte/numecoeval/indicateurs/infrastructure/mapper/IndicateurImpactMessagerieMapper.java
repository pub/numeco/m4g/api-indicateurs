package org.mte.numecoeval.indicateurs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactMessagerie;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactMessagerieEntity;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactMessagerieDTO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Mapper(componentModel = "spring")
public interface IndicateurImpactMessagerieMapper {
    IndicateurImpactMessagerie toIndicateurImpactMessagerie(IndicateurImpactMessagerieDTO dto);

    @Mapping(target = "moisAnnee", source = "moisAnnee",qualifiedByName = "convertToLocalDate")
    IndicateurImpactMessagerieEntity toIndicateurImpactMessagerieEntity(IndicateurImpactMessagerie indicateurImpactMessagerie);

    @Named("convertToLocalDate")
    default LocalDate convertToLocalDate(Integer moisAnnee){

        if (moisAnnee!=null){
            try {
                return LocalDate.parse(moisAnnee + "01", DateTimeFormatter.ofPattern("yyyyMMdd"));
            }catch (DateTimeParseException e){
                //
            }
        }
        return null;
    }
}
