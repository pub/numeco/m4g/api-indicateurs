package org.mte.numecoeval.indicateurs.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactEquipementVirtuelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource( path = "impactEquipementVirtuel")
@Tag(name = "Indicateur Impact Equipement Virtuel")
public interface IndicateurImpactEquipementVirtuelRepository extends JpaRepository<IndicateurImpactEquipementVirtuelEntity,Long> {
}
