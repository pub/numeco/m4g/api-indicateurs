package org.mte.numecoeval.indicateurs.infrastructure.jpa.adapter;

import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactEquipementPhysique;
import org.mte.numecoeval.indicateurs.domain.port.output.IndicateurPersistencePort;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactEquipementPhysiqueRepository;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactEquipementMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("indicateurImpactEquipementPhysiqueJpaAdapter")
public class IndicateurImpactEquipementPhysiqueJpaAdapter implements IndicateurPersistencePort<IndicateurImpactEquipementPhysique> {

    private IndicateurImpactEquipementPhysiqueRepository indicateurImpactEquipementPhysiqueRepository;
    private IndicateurImpactEquipementMapper indicateurImpactEquipementMapper;

    public IndicateurImpactEquipementPhysiqueJpaAdapter(IndicateurImpactEquipementPhysiqueRepository indicateurImpactEquipementPhysiqueRepository, IndicateurImpactEquipementMapper indicateurImpactEquipementMapper) {
        this.indicateurImpactEquipementPhysiqueRepository = indicateurImpactEquipementPhysiqueRepository;
        this.indicateurImpactEquipementMapper = indicateurImpactEquipementMapper;
    }

    @Override
    public void save(IndicateurImpactEquipementPhysique indicateurImpactEquipementPhysique) {
        if(indicateurImpactEquipementPhysique !=null){
            indicateurImpactEquipementPhysiqueRepository.save(indicateurImpactEquipementMapper.toIndicateurImpactEquipementPhysiqueEntity(indicateurImpactEquipementPhysique));
        }
    }

    @Override
    public void saveAll(List<IndicateurImpactEquipementPhysique> indicateurs) {
        if (CollectionUtils.isNotEmpty(indicateurs)){
            indicateurImpactEquipementPhysiqueRepository.saveAll(indicateurs.stream()
                    .map(indicteur-> indicateurImpactEquipementMapper.toIndicateurImpactEquipementPhysiqueEntity(indicteur))
                    .toList());
        }
    }

    @Override
    public void deleteAll() {
        indicateurImpactEquipementPhysiqueRepository.deleteAll();
    }
}
