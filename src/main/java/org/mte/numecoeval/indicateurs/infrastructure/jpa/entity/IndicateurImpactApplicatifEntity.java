package org.mte.numecoeval.indicateurs.infrastructure.jpa.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Table(name = "IND_INDICATEUR_IMPACT_APPLICATION")
@Entity
@Schema(
        description = "Indicateur d'impact écologique d'une application"
)
public class IndicateurImpactApplicatifEntity extends AbstractIndicateurImpactEntity{

    /**
     * Identifiant technique de l'indicateur
     */
    @Id
    @GeneratedValue(generator = "SEQ_INDICATEUR_IMPACT_APPLICATION", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_INDICATEUR_IMPACT_APPLICATION", sequenceName="SEQ_INDICATEUR_IMPACT_APPLICATION",allocationSize=1000)
    @Column(nullable = false)
    protected Long id;
    /**
     * Nom de l'application associée à l'indicateur
     */
    private String nomApplication;
    /**
     * Impact unitaire de l'application
     */
    private Double impactUnitaire;
    /**
     * Domaine de l'application
     */
    private String domaine;
    /**
     * Sous domaine de l'application
     */
    private String sousDomaine;
    /**
     * Type de l'environnement associé à l'application
     */
    private String typeEnvironnement;
    /**
     * Consommation électrique moyenne de l'application
     */
    private Double consoElecMoyenne;
}
