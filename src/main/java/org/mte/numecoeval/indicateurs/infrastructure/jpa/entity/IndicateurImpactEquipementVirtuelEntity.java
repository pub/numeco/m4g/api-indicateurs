package org.mte.numecoeval.indicateurs.infrastructure.jpa.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Table(name = "IND_INDICATEUR_IMPACT_EQUIPEMENT_VIRTUEL")
@Entity
@Schema(
        description = "Indicateur d'impact écologique d'un équipement virtuel"
)
public class IndicateurImpactEquipementVirtuelEntity extends AbstractIndicateurImpactEntity {
    /**
     * Identifiant technique de l'indicateur
     */
    @Id
    @GeneratedValue(generator = "SEQ_INDICATEUR_IMPACT_EQUIPEMENT_VIRTUEL", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_INDICATEUR_IMPACT_EQUIPEMENT_VIRTUEL", sequenceName = "SEQ_INDICATEUR_IMPACT_EQUIPEMENT_VIRTUEL", allocationSize = 1000)
    @Column(nullable = false)
    private Long id;

    /**
     * Nom de la VM
     */
    @Column(name = "nom_vm")
    private String nomVM;
    /**
     * Nom de l'équipement physique associé à la VM
     */
    private String nomEquipement;
    private Double impactUnitaire;
    private String cluster;
    private Double consoElecMoyenne;
}
