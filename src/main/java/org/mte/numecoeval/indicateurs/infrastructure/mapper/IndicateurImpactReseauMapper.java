package org.mte.numecoeval.indicateurs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactReseau;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactReseauEntity;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactReseauDTO;

import java.util.Objects;

@Mapper(componentModel = "spring")
public interface IndicateurImpactReseauMapper {

    @Mapping(target = "statutIndicateur", source = "statutIndicateur", defaultExpression = "java(this.getDefaultStatut(indicateurImpactReseauDTO))")
    IndicateurImpactReseau toIndicateurImpactReseau(IndicateurImpactReseauDTO indicateurImpactReseauDTO);

    IndicateurImpactReseauEntity toIndicateurImpactReseauEntity(IndicateurImpactReseau indicateurImpactReseau);

    default String getDefaultStatut(IndicateurImpactReseauDTO indicateurImpactReseauDTO) {
        if (Objects.nonNull(indicateurImpactReseauDTO.getImpactUnitaire()))
            return "OK";

        return "ERREUR";
    }
}
