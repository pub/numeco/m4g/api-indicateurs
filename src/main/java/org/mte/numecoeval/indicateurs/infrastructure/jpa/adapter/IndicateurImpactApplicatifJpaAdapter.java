package org.mte.numecoeval.indicateurs.infrastructure.jpa.adapter;


import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactApplicatif;
import org.mte.numecoeval.indicateurs.domain.port.output.IndicateurPersistencePort;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactApplicatifRepository;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactApplicatifMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndicateurImpactApplicatifJpaAdapter implements IndicateurPersistencePort<IndicateurImpactApplicatif> {

    private IndicateurImpactApplicatifRepository indicateurImpactApplicatifRepository;
    private IndicateurImpactApplicatifMapper indicateurImpactApplicatifMapper;

    public IndicateurImpactApplicatifJpaAdapter(IndicateurImpactApplicatifRepository indicateurImpactApplicatifRepository, IndicateurImpactApplicatifMapper indicateurImpactApplicatifMapper) {
        this.indicateurImpactApplicatifRepository = indicateurImpactApplicatifRepository;
        this.indicateurImpactApplicatifMapper = indicateurImpactApplicatifMapper;
    }

    @Override
    public void save(IndicateurImpactApplicatif indicateurImpactApplicatif) {
        if(indicateurImpactApplicatif!=null){
            indicateurImpactApplicatifRepository.save(indicateurImpactApplicatifMapper.toIndicateurImpactApplicatifEntity(indicateurImpactApplicatif));
        }
    }

    @Override
    public void saveAll(List<IndicateurImpactApplicatif> indicateurImpactApplicatifs) {
        if(CollectionUtils.isNotEmpty(indicateurImpactApplicatifs)){
            indicateurImpactApplicatifRepository.saveAll(indicateurImpactApplicatifs.stream()
                    .map(indicateurImpactApplicatifMapper::toIndicateurImpactApplicatifEntity)
                    .toList());
        }
    }

    @Override
    public void deleteAll() {
        indicateurImpactApplicatifRepository.deleteAll();
    }
}
