package org.mte.numecoeval.indicateurs.infrastructure.jpa.adapter;

import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactReseau;
import org.mte.numecoeval.indicateurs.domain.port.output.IndicateurPersistencePort;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactReseauRepository;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactReseauMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndicateurImpactReseauJpaAdapter implements IndicateurPersistencePort<IndicateurImpactReseau> {

    private IndicateurImpactReseauRepository indicateurImpactReseauRepository;
    private IndicateurImpactReseauMapper indicateurImpactReseauMapper;

    public IndicateurImpactReseauJpaAdapter(IndicateurImpactReseauRepository indicateurImpactReseauRepository, IndicateurImpactReseauMapper indicateurImpactReseauMapper) {
        this.indicateurImpactReseauRepository = indicateurImpactReseauRepository;
        this.indicateurImpactReseauMapper = indicateurImpactReseauMapper;
    }

    @Override
    public void save(IndicateurImpactReseau indicateurImpactReseau) {
        if(indicateurImpactReseau !=null){
            indicateurImpactReseauRepository.save(indicateurImpactReseauMapper.toIndicateurImpactReseauEntity(indicateurImpactReseau));
        }
    }

    @Override
    public void saveAll(List<IndicateurImpactReseau> indicateursIndicateurImpactReseau) {
        if (CollectionUtils.isNotEmpty(indicateursIndicateurImpactReseau)){
            indicateurImpactReseauRepository.saveAll(indicateursIndicateurImpactReseau.stream()
                    .map(indicteur->indicateurImpactReseauMapper.toIndicateurImpactReseauEntity(indicteur))
                    .toList());
        }
    }

    @Override
    public void deleteAll() {
        indicateurImpactReseauRepository.deleteAll();
    }
}
