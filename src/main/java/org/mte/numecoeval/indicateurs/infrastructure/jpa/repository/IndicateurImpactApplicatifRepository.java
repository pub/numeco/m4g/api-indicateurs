package org.mte.numecoeval.indicateurs.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactApplicatifEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource( path = "impactApplicatif")
@Tag(name = "Indicateur Impact Application", description = "Opérations pour les impacts écologiques d'applications")
public interface IndicateurImpactApplicatifRepository extends JpaRepository<IndicateurImpactApplicatifEntity,Long> {
}
