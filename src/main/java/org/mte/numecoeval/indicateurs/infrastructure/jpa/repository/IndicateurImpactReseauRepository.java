package org.mte.numecoeval.indicateurs.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactReseauEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource( path = "impactReseau" )
@Tag(name = "Indicateur Impact Reseau")
public interface IndicateurImpactReseauRepository extends JpaRepository<IndicateurImpactReseauEntity,Long> {
}
