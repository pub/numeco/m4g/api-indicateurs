package org.mte.numecoeval.indicateurs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactEquipementPhysique;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactEquipementVirtuel;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactEquipementPhysiqueEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactEquipementVirtuelEntity;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactEquipementVirtuelDTO;

import java.util.Objects;

@Mapper(componentModel = "spring")
public interface IndicateurImpactEquipementMapper {

    @Mapping(target = "statutIndicateur", source = "statutIndicateur", defaultExpression = "java(this.getDefaultStatut(indicateurImpactEquipementPhysiqueDTO))")
    IndicateurImpactEquipementPhysique toIndicateurImpactEquipementPhysique(IndicateurImpactEquipementPhysiqueDTO indicateurImpactEquipementPhysiqueDTO);

    IndicateurImpactEquipementPhysiqueEntity toIndicateurImpactEquipementPhysiqueEntity(IndicateurImpactEquipementPhysique indicateurImpactEquipementPhysique);

    IndicateurImpactEquipementVirtuel toIndicateurImpactEquipementVirtuel(IndicateurImpactEquipementVirtuelDTO indicateurImpactEquipementVirtuelDTO);
    IndicateurImpactEquipementVirtuelEntity toIndicateurImpactEquipementVirtuelEntity(IndicateurImpactEquipementVirtuel indicateurImpactEquipementVirtuel);
    default String getDefaultStatut(IndicateurImpactEquipementPhysiqueDTO indicateurImpactEquipementPhysiqueDTO) {
        if (Objects.nonNull(indicateurImpactEquipementPhysiqueDTO.getImpactUnitaire())
                || Objects.nonNull(indicateurImpactEquipementPhysiqueDTO.getConsoElecMoyenne()))
            return "OK";

        return "ERREUR";
    }
}
