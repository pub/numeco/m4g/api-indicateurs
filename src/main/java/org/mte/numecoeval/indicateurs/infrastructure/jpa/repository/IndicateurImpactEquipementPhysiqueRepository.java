package org.mte.numecoeval.indicateurs.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactEquipementPhysiqueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource( path = "impactEquipementPhysique")
@Tag(name = "Indicateur Impact Equipement Physique")
public interface IndicateurImpactEquipementPhysiqueRepository extends JpaRepository<IndicateurImpactEquipementPhysiqueEntity,Long> {
}
