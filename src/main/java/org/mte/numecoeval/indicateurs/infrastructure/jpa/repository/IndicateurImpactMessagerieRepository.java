package org.mte.numecoeval.indicateurs.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactMessagerieEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource( path = "impactMessagerie")
@Tag(name = "Indicateur Impact Messagerie")
public interface IndicateurImpactMessagerieRepository  extends JpaRepository<IndicateurImpactMessagerieEntity,Long> {
}
