package org.mte.numecoeval.indicateurs.infrastructure.configuration;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactApplicatifEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactEquipementPhysiqueEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactEquipementVirtuelEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactMessagerieEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactReseauEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.mapping.ExposureConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
public class ApiIndicateurRestConfig implements RepositoryRestConfigurer {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
        ExposureConfiguration exposureConfiguration = config.getExposureConfiguration();
        exposureConfiguration.forDomainType(IndicateurImpactReseauEntity.class)
                .withItemExposure((metadata, httpMethods) -> httpMethods.disable(
                        HttpMethod.POST, HttpMethod.DELETE, HttpMethod.PATCH, HttpMethod.PUT
                ));
        exposureConfiguration.forDomainType(IndicateurImpactEquipementPhysiqueEntity.class)
                .withItemExposure((metadata, httpMethods) -> httpMethods.disable(
                        HttpMethod.POST, HttpMethod.DELETE, HttpMethod.PATCH, HttpMethod.PUT
                ));
        exposureConfiguration.forDomainType(IndicateurImpactEquipementVirtuelEntity.class)
                .withItemExposure((metadata, httpMethods) -> httpMethods.disable(
                        HttpMethod.POST, HttpMethod.DELETE, HttpMethod.PATCH, HttpMethod.PUT
                ));
        exposureConfiguration.forDomainType(IndicateurImpactMessagerieEntity.class)
                .withItemExposure((metadata, httpMethods) -> httpMethods.disable(
                        HttpMethod.POST, HttpMethod.DELETE, HttpMethod.PATCH, HttpMethod.PUT
                ));
        exposureConfiguration.forDomainType(IndicateurImpactApplicatifEntity.class)
                .withItemExposure((metadata, httpMethods) -> httpMethods.disable(
                        HttpMethod.POST, HttpMethod.DELETE, HttpMethod.PATCH, HttpMethod.PUT
                ));
    }

    @Bean
    public OpenAPI configOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("API des indicateurs de NumEcoEval")
                        .description("""
                                Endpoints permettant de manipuler les indicateurs de NumEcoEval.
                                Les endpoints CRUD sont générés via Spring DataRest.
                                Les endpoints de purge sont des opérations longues qui peuvent prendre du temps.
                                """)
                        .version("v0.1")
                        .license(new License()
                                .name("Apache 2.0")
                                .url("https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g")
                        )
                )
                .externalDocs(new ExternalDocumentation()
                        .description("NumEcoEval Documentation")
                        .url("https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/docs"));
    }
}
