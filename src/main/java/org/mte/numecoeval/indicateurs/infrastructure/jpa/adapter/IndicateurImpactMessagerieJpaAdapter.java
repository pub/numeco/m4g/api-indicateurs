package org.mte.numecoeval.indicateurs.infrastructure.jpa.adapter;

import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactMessagerie;
import org.mte.numecoeval.indicateurs.domain.port.output.IndicateurPersistencePort;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactMessagerieRepository;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactMessagerieMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndicateurImpactMessagerieJpaAdapter implements IndicateurPersistencePort<IndicateurImpactMessagerie> {

    private IndicateurImpactMessagerieRepository indicateurImpactMessagerieRepository;
    private IndicateurImpactMessagerieMapper indicateurImpactMessagerieMapper;

    public IndicateurImpactMessagerieJpaAdapter(IndicateurImpactMessagerieRepository indicateurImpactMessagerieRepository, IndicateurImpactMessagerieMapper indicateurImpactMessagerieMapper) {
        this.indicateurImpactMessagerieRepository = indicateurImpactMessagerieRepository;
        this.indicateurImpactMessagerieMapper = indicateurImpactMessagerieMapper;
    }

    @Override
    public void save(IndicateurImpactMessagerie indicateur) {
        if (indicateur!=null){
            indicateurImpactMessagerieRepository.save(indicateurImpactMessagerieMapper.toIndicateurImpactMessagerieEntity(indicateur));
        }
    }

    @Override
    public void saveAll(List<IndicateurImpactMessagerie> indicateurs) {
       if(CollectionUtils.isNotEmpty(indicateurs)){
           indicateurImpactMessagerieRepository.saveAll(indicateurs.stream()
                   .map(indicateur->indicateurImpactMessagerieMapper.toIndicateurImpactMessagerieEntity(indicateur))
                   .toList());
       }
    }

    @Override
    public void deleteAll() {
        indicateurImpactMessagerieRepository.deleteAll();
    }
}
