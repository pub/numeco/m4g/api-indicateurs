package org.mte.numecoeval.indicateurs.infrastructure.jpa.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@Table(name = "IND_INDICATEUR_IMPACT_EQUIPEMENT_PHYSIQUE")
@Entity
@Schema(
        description = "Indicateur d'impact écologique d'un équipement physique"
)
public class IndicateurImpactEquipementPhysiqueEntity extends AbstractIndicateurImpactEntity {

    /**
     * Identifiant technique de l'indicateur
     */
    @Id
    @GeneratedValue(generator = "SEQ_INDICATEUR_IMPACT_EQUIPEMENT_PHYSIQUE", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_INDICATEUR_IMPACT_EQUIPEMENT_PHYSIQUE", sequenceName="SEQ_INDICATEUR_IMPACT_EQUIPEMENT_PHYSIQUE",allocationSize=1000)
    @Column(nullable = false)
    private Long id;
    /**
     * Référence d'équipement physique utilisé dans le calcul de l'indicateur
     */
    private String reference;
    /**
     * Nom de l'équipement physique associé
     */
    private String nomEquipement;
    /**
     * Type d'équipement physique
     */
    private String typeEquipement;
    /**
     * Impact unitaire de l'équipement physique
     */
    private Double impactUnitaire;
    /**
     * Consommation électrique moyenne de l'équipement physique
     */
    private Double consoElecMoyenne;
    /**
     * Quantité d'équipements associés
     */
    Integer quantite;
    /**
     * Flag indiquant si l'indicateur concerne un équipement virtuel
     */
    boolean estUnEquipementVirtuel;
    /**
     * Statut de l'équipement physique associé
     */
    String statutEquipementPhysique;
}
