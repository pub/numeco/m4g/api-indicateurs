package org.mte.numecoeval.indicateurs.infrastructure.jpa.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@MappedSuperclass
public class AbstractIndicateurImpactEntity {

    /**
     * Date et Heure du calcul, même valeur pour tous les indicateurs créés avec le même lot d'objets d'entrées
     */
    @Schema(
            description = "Date et Heure du calcul, même valeur pour tous les indicateurs créés avec le même lot d'objets d'entrées"
    )
    protected LocalDateTime dateCalcul;
    protected String referentielsSources;
    protected String versionCalcul; // Version Projet
    protected String versionReferentiel;//version Réferentiel ( date ..? )
    /**
     * Code de l'étape ACV associé à  l'indicateur
     */
    @Schema(
            description = "Code de l'étape ACV associé à l'indicateur, peut être null pour certains indicateurs"
    )
    protected String etapeACV;
    /**
     * Nom du critère associé à l'indicateur, peut être {@code null} pour certains indicateurs
     */
    @Schema(
            description = "Nom du critère associé à l'indicateur, peut être null pour certains indicateurs"
    )
    protected String critere;
    protected String source;
    /**
     * Statut de l'indicateur, vaut "OK" si l'indicateur est calcul et "ERREUR" si l'indicateur est en erreur
     */
    @Schema(
            description = "Statut de l'indicateur, vaut \"OK\" si l'indicateur est calcul et \"ERREUR\" si l'indicateur est en erreur"
    )
    protected String statutIndicateur;
    @Schema(
            description = "Trace du calcul ayant produit l'indicateur"
    )
    @Column(columnDefinition="TEXT")
    protected String trace;
    @Schema(
            description = "Date de lot de rattachement de l'indicateur"
    )
    protected LocalDate dateLot;
    /**
     * Unite du critère associé, peut être {@code null}
     */
    @Schema(
            description = "Unite du critère associé, peut être null"
    )
    protected String unite;
    /**
     * Nom de l'organisation - Metadata
     */
    @Schema(
            description = "Nom de l'organisation - Metadata"
    )
    protected String nomOrganisation;
    /**
     * Nom de l'entité pouvant agir sur l'indicateur - Metadata
     */
    @Schema(
            description = "Nom de l'entité pouvant agir sur l'indicateur - Metadata"
    )
    protected String nomEntite;
}
