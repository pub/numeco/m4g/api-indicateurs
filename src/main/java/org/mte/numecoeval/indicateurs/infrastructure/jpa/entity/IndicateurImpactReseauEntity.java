package org.mte.numecoeval.indicateurs.infrastructure.jpa.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Table(name = "IND_INDICATEUR_IMPACT_RESEAU")
@Entity
@Schema(
        description = "Indicateur d'impact écologique lié à l'usage du réseau par un équipement physique"
)
public class IndicateurImpactReseauEntity extends AbstractIndicateurImpactEntity {

    /**
     * Identifiant technique de l'indicateur
     */
    @Id
    @GeneratedValue(generator = "SEQ_INDICATEUR_IMPACT_RESEAU", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_INDICATEUR_IMPACT_RESEAU", sequenceName="SEQ_INDICATEUR_IMPACT_RESEAU",allocationSize=1000)
    @Column(nullable = false)
    protected Long id;
    private Double impactUnitaire;
    private String reference;
}
