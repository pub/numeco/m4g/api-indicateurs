package org.mte.numecoeval.indicateurs.infrastructure.jpa.adapter;

import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactEquipementVirtuel;
import org.mte.numecoeval.indicateurs.domain.port.output.IndicateurPersistencePort;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactEquipementVirtuelRepository;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactEquipementMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("indicateurImpactEquipementVirtuelJpaAdapter")
public class IndicateurImpactEquipementVirtuelJpaAdapter  implements IndicateurPersistencePort<IndicateurImpactEquipementVirtuel> {

    private final IndicateurImpactEquipementVirtuelRepository indicateurImpactEquipementVirtuelRepository;
    private final IndicateurImpactEquipementMapper indicateurImpactEquipementMapper;

    public IndicateurImpactEquipementVirtuelJpaAdapter(IndicateurImpactEquipementVirtuelRepository indicateurImpactEquipementVirtuelRepository, IndicateurImpactEquipementMapper indicateurImpactEquipementMapper) {
        this.indicateurImpactEquipementVirtuelRepository = indicateurImpactEquipementVirtuelRepository;
        this.indicateurImpactEquipementMapper = indicateurImpactEquipementMapper;
    }

    @Override
    public void save(IndicateurImpactEquipementVirtuel indicateur) {
        if (indicateur!=null){
            indicateurImpactEquipementVirtuelRepository.save(indicateurImpactEquipementMapper.toIndicateurImpactEquipementVirtuelEntity(indicateur));
        }
    }

    @Override
    public void saveAll(List<IndicateurImpactEquipementVirtuel> indicateurs) {
        if (CollectionUtils.isNotEmpty(indicateurs)){
            indicateurImpactEquipementVirtuelRepository.saveAll(indicateurs.stream()
                    .map(indicateurImpactEquipementMapper::toIndicateurImpactEquipementVirtuelEntity).toList());
        }
    }

    @Override
    public void deleteAll() {
        indicateurImpactEquipementVirtuelRepository.deleteAll();
    }
}
