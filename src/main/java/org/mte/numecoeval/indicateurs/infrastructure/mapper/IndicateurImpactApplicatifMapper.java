package org.mte.numecoeval.indicateurs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactApplicatif;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactApplicatifEntity;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactApplicationDTO;

import java.util.Objects;

@Mapper(componentModel = "spring")
public interface IndicateurImpactApplicatifMapper {

    @Mapping(target = "statutIndicateur", source = "statutIndicateur", defaultExpression = "java(this.getDefaultStatut(indicateurImpactApplicationDTO))")
    IndicateurImpactApplicatif toIndicateurImpactApplicatif(IndicateurImpactApplicationDTO indicateurImpactApplicationDTO);
    IndicateurImpactApplicatifEntity toIndicateurImpactApplicatifEntity(IndicateurImpactApplicatif indicateurImpactApplicatif);

    default String getDefaultStatut(IndicateurImpactApplicationDTO indicateurImpactApplicationDTO) {
        if (Objects.nonNull(indicateurImpactApplicationDTO.getImpactUnitaire()))
            return "OK";

        return "ERREUR";
    }
}
