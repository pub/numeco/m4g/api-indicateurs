package org.mte.numecoeval.indicateurs.domain.port.output;

import org.mte.numecoeval.indicateurs.domain.model.AbstractIndicateurImpact;

import java.util.List;

public interface IndicateurPersistencePort<T extends AbstractIndicateurImpact> {

     void save(T indicateur);
     void saveAll(List<T> indicateurs);
     void deleteAll();

}
