package org.mte.numecoeval.indicateurs.domain.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class IndicateurImpactMessagerie extends AbstractIndicateurImpact{
    private Double impactMensuel;
    private Integer moisAnnee;
    private Double volumeTotalMailEmis;
    private Double nombreMailEmis;
}
