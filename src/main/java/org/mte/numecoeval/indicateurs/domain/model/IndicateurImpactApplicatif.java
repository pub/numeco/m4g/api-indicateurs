package org.mte.numecoeval.indicateurs.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@Getter
@Setter
public class IndicateurImpactApplicatif extends AbstractIndicateurImpact{
    private String nomApplication;
    private Double impactUnitaire;
    private String domaine;
    private String sousDomaine;
    private String typeEnvironnement;
    private Double consoElecMoyenne;
}
