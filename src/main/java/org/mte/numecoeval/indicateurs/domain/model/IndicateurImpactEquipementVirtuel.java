package org.mte.numecoeval.indicateurs.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@EqualsAndHashCode
public class IndicateurImpactEquipementVirtuel extends AbstractIndicateurImpact{
    String nomVM;
    String nomEquipement;
    Double impactUnitaire;
    String cluster;
    Double consoElecMoyenne;
}
