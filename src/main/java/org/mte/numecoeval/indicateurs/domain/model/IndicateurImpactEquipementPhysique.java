package org.mte.numecoeval.indicateurs.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class IndicateurImpactEquipementPhysique extends AbstractIndicateurImpact{

    String reference;
    String nomEquipement;
    String typeEquipement;
    Double impactUnitaire;
    Double consoElecMoyenne;
    Integer quantite;
    boolean estUnEquipementVirtuel;
    String statutEquipementPhysique;
}
