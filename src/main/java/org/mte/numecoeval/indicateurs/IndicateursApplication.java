package org.mte.numecoeval.indicateurs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndicateursApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndicateursApplication.class, args);
	}

}
