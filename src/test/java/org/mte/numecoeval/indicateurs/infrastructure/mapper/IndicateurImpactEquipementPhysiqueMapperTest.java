package org.mte.numecoeval.indicateurs.infrastructure.mapper;

import org.junit.jupiter.api.Test;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactEquipementPhysiqueDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IndicateurImpactEquipementPhysiqueMapperTest {

    private IndicateurImpactEquipementMapper mapper = new IndicateurImpactEquipementMapperImpl();

    @Test
    void toIndicateurImpactEquipement_shouldInitializeStatutWithValueErreur() {
        var dto = IndicateurImpactEquipementPhysiqueDTO.builder()
                .statutIndicateur(null)
                .impactUnitaire(null)
                .consoElecMoyenne(null)
                .build();

        var result = mapper.toIndicateurImpactEquipementPhysique(dto);

        assertEquals("ERREUR", result.getStatutIndicateur());
    }

    @Test
    void toIndicateurImpactEquipement_whenImpactUnitaireNotNull_shouldInitializeStatutWithValueOK() {
        var dto = IndicateurImpactEquipementPhysiqueDTO.builder()
                .statutIndicateur(null)
                .impactUnitaire(2.1)
                .consoElecMoyenne(null)
                .build();

        var result = mapper.toIndicateurImpactEquipementPhysique(dto);

        assertEquals("OK", result.getStatutIndicateur());
    }

    @Test
    void toIndicateurImpactEquipement_whenConsoElecAnMoyenneNotNull_shouldInitializeStatutWithValueOK() {
        var dto = IndicateurImpactEquipementPhysiqueDTO.builder()
                .statutIndicateur(null)
                .impactUnitaire(null)
                .consoElecMoyenne(1.2)
                .build();

        var result = mapper.toIndicateurImpactEquipementPhysique(dto);

        assertEquals("OK", result.getStatutIndicateur());
    }
}
