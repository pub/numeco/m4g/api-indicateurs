package org.mte.numecoeval.indicateurs.infrastructure.mapper;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactMessagerie;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactMessagerieDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

 class IndicateurImpactMessagerieMapperTest {

    private IndicateurImpactMessagerieMapper mapper = new IndicateurImpactMessagerieMapperImpl();

    @Test
    void shouldConvertToIndicateurImpactMessagerieEntity(){

        String critre= "Changement climatique";
        LocalDate expectedDate = LocalDate.parse("20220701", DateTimeFormatter.ofPattern("yyyyMMdd"));
        var indicateur = IndicateurImpactMessagerie.builder()
                .impactMensuel(32d)
                .moisAnnee(202207)
                .critere(critre)
                .build();
        var actual = mapper.toIndicateurImpactMessagerieEntity(indicateur);

        assertEquals(expectedDate, actual.getMoisAnnee());
        assertEquals(32d,actual.getImpactMensuel());
        assertEquals(critre,actual.getCritere());
    }

    @Test
    void shouldConvertToIndicateurImpactMessagerieEntity_whenInvalidMoisAnnee(){
        var indicateur = IndicateurImpactMessagerie.builder()
                .impactMensuel(32d)
                .moisAnnee(RandomUtils.nextInt(10,99))
                .build();
        var actual = mapper.toIndicateurImpactMessagerieEntity(indicateur);
        assertEquals(null, actual.getMoisAnnee());
    }
    @Test
    void shouldConvertToIndicateurImpactMessagerie(){
        String critre= "Changement climatique";
        LocalDateTime now = LocalDateTime.now();
        var indicateurDto = IndicateurImpactMessagerieDTO.builder()
                .impactMensuel(32d)
                .moisAnnee(202207)
                .critere(critre)
                .dateCalcul(now)
                .build();

        var actual = mapper.toIndicateurImpactMessagerie(indicateurDto);
        assertEquals(critre,actual.getCritere());
        assertEquals(now,actual.getDateCalcul());
        assertEquals(32d,actual.getImpactMensuel());
        assertEquals(202207,actual.getMoisAnnee());

    }
}
