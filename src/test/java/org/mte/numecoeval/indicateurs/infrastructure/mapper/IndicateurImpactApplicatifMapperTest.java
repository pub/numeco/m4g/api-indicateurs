package org.mte.numecoeval.indicateurs.infrastructure.mapper;

import org.junit.jupiter.api.Test;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactApplicationDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IndicateurImpactApplicatifMapperTest {

    private IndicateurImpactApplicatifMapper mapper = new IndicateurImpactApplicatifMapperImpl();

    @Test
    void toIndicateurImpactEquipement_shouldInitializeStatutWithValueErreur() {
        var dto = IndicateurImpactApplicationDTO.builder()
                .statutIndicateur(null)
                .impactUnitaire(null)
                .build();

        var result = mapper.toIndicateurImpactApplicatif(dto);

        assertEquals("ERREUR", result.getStatutIndicateur());
    }

    @Test
    void toIndicateurImpactEquipement_whenImpactUnitaireNotNull_shouldInitializeStatutWithValueOK() {
        var dto = IndicateurImpactApplicationDTO.builder()
                .statutIndicateur(null)
                .impactUnitaire(2.1)
                .build();

        var result = mapper.toIndicateurImpactApplicatif(dto);

        assertEquals("OK", result.getStatutIndicateur());
    }

}
