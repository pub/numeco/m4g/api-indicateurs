package org.mte.numecoeval.indicateurs.infrastructure.mapper;

import org.junit.jupiter.api.Test;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactReseauDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IndicateurImpactReseauMapperTest {

    private IndicateurImpactReseauMapper mapper = new IndicateurImpactReseauMapperImpl();

    @Test
    void toIndicateurImpactEquipement_shouldInitializeStatutWithValueErreur() {
        var dto = IndicateurImpactReseauDTO.builder()
                .statutIndicateur(null)
                .impactUnitaire(null)
                .build();

        var result = mapper.toIndicateurImpactReseau(dto);

        assertEquals("ERREUR", result.getStatutIndicateur());
    }

    @Test
    void toIndicateurImpactEquipement_whenImpactUnitaireNotNull_shouldInitializeStatutWithValueOK() {
        var dto = IndicateurImpactReseauDTO.builder()
                .statutIndicateur(null)
                .impactUnitaire(2.1)
                .build();

        var result = mapper.toIndicateurImpactReseau(dto);

        assertEquals("OK", result.getStatutIndicateur());
    }

}
