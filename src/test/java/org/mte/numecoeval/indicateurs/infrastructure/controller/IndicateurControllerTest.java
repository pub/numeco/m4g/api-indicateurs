package org.mte.numecoeval.indicateurs.infrastructure.controller;

import lombok.extern.slf4j.Slf4j;
import org.instancio.Instancio;
import org.instancio.Select;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactApplicatifEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactEquipementPhysiqueEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactEquipementVirtuelEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactMessagerieEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactReseauEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactApplicatifRepository;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactEquipementPhysiqueRepository;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactEquipementVirtuelRepository;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactMessagerieRepository;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactReseauRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(profiles = { "test" })
@Slf4j
class IndicateurControllerTest {
    @Autowired
    private IndicateurImpactApplicatifRepository indicateurImpactApplicatifRepository;
    @Autowired
    private IndicateurImpactMessagerieRepository indicateurImpactMessagerieRepository;
    @Autowired
    private IndicateurImpactReseauRepository indicateurImpactReseauRepository;
    @Autowired
    private IndicateurImpactEquipementPhysiqueRepository indicateurImpactEquipementPhysiqueRepository;
    @Autowired
    private IndicateurImpactEquipementVirtuelRepository indicateurImpactEquipementVirtuelRepository;

    @LocalServerPort
    private Integer port;
    RestTemplate restTemplate = new RestTemplate();

    private String createURLWithPort(String uri) {
        String baseUrl = "http://localhost";
        return baseUrl.concat(":").concat(port.toString()).concat(uri);
    }
    @Test
    void shouldDeleteImpactEquipementsPhysiqueWhenCallDeleteAPI() {

        String uri = "/indicateur/indicateursEquipementPhysiques";
        String url = createURLWithPort(uri);
        IndicateurImpactEquipementPhysiqueEntity equipementPhysique1 = Instancio.of(IndicateurImpactEquipementPhysiqueEntity.class)
                .set(Select.field("estUnEquipementVirtuel"), false)
                .create();
        IndicateurImpactEquipementPhysiqueEntity equipementPhysique2 = Instancio.of(IndicateurImpactEquipementPhysiqueEntity.class)
                .set(Select.field("estUnEquipementVirtuel"), false)
                .create();
        IndicateurImpactEquipementPhysiqueEntity equipementPhysique3 = Instancio.of(IndicateurImpactEquipementPhysiqueEntity.class)
                .set(Select.field("estUnEquipementVirtuel"), false)
                .create();
        IndicateurImpactEquipementPhysiqueEntity equipementVirtuel1 = Instancio.of(IndicateurImpactEquipementPhysiqueEntity.class)
                .create();
        IndicateurImpactEquipementPhysiqueEntity equipementVirtuel2 = Instancio.of(IndicateurImpactEquipementPhysiqueEntity.class)
                .create();
        indicateurImpactEquipementPhysiqueRepository.saveAll(Arrays.asList(equipementPhysique1, equipementPhysique2, equipementPhysique3, equipementVirtuel1, equipementVirtuel2));

        assertEquals(5, indicateurImpactEquipementPhysiqueRepository.findAll().size());
        restTemplate.delete(url);
        assertEquals(0, indicateurImpactEquipementPhysiqueRepository.findAll().size());
    }
    @Test
    void shouldDeleteImpactEquipementsVirutelsWhenCallDeleteAPI() {

        String uri = "/indicateur/indicateursEquipementVirtuels";
        String url = createURLWithPort(uri);
        IndicateurImpactEquipementVirtuelEntity equipement1 = Instancio.of(IndicateurImpactEquipementVirtuelEntity.class)
                .create();
        IndicateurImpactEquipementVirtuelEntity equipement2 = Instancio.of(IndicateurImpactEquipementVirtuelEntity.class)
                .create();
        IndicateurImpactEquipementVirtuelEntity equipement3 = Instancio.of(IndicateurImpactEquipementVirtuelEntity.class)
                .create();
        IndicateurImpactEquipementVirtuelEntity equipement4 = Instancio.of(IndicateurImpactEquipementVirtuelEntity.class)
                .create();
        IndicateurImpactEquipementVirtuelEntity equipement5 = Instancio.of(IndicateurImpactEquipementVirtuelEntity.class)
                .create();
        indicateurImpactEquipementVirtuelRepository.saveAll(Arrays.asList(equipement1, equipement2, equipement3, equipement4, equipement5));
        assertEquals(5, indicateurImpactEquipementVirtuelRepository.findAll().size());
        restTemplate.delete(url);
        assertEquals(0, indicateurImpactEquipementVirtuelRepository.findAll().size());
    }
    @Test
    void shouldDeleteImpactReseaux() {
        String uri = "/indicateur/indicateursImpactsReseaux";
        String url = createURLWithPort(uri);

        IndicateurImpactReseauEntity impactReseau1 = Instancio.create(IndicateurImpactReseauEntity.class);
        IndicateurImpactReseauEntity impactReseau2 = Instancio.create(IndicateurImpactReseauEntity.class);
        IndicateurImpactReseauEntity impactReseau3 = Instancio.create(IndicateurImpactReseauEntity.class);

        indicateurImpactReseauRepository.saveAll(Arrays.asList(impactReseau1, impactReseau2, impactReseau3));
        assertEquals(3, indicateurImpactReseauRepository.findAll().size());
        restTemplate.delete(url);
        assertEquals(0, indicateurImpactReseauRepository.findAll().size());
    }
    @Test
    void shouldDeleteImpactMessagerie() {
        String uri = "/indicateur/indicateursImpactMessageries";
        String url = createURLWithPort(uri);

        IndicateurImpactMessagerieEntity impactMessagerie1 = Instancio.create(IndicateurImpactMessagerieEntity.class);
        IndicateurImpactMessagerieEntity impactMessagerie2 = Instancio.create(IndicateurImpactMessagerieEntity.class);
        IndicateurImpactMessagerieEntity impactMessagerie3 = Instancio.create(IndicateurImpactMessagerieEntity.class);
        IndicateurImpactMessagerieEntity impactMessagerie4 = Instancio.create(IndicateurImpactMessagerieEntity.class);

        indicateurImpactMessagerieRepository.saveAll(Arrays.asList(impactMessagerie1, impactMessagerie2, impactMessagerie3, impactMessagerie4));
        assertEquals(4, indicateurImpactMessagerieRepository.findAll().size());
        restTemplate.delete(url);
        assertEquals(0, indicateurImpactMessagerieRepository.findAll().size());
    }

    @Test
    void shouldDeleteAllImpactsApplicatifs() {
        String uri = "/indicateur/indicateursImpactApplicatifs";
        String url = createURLWithPort(uri);

        IndicateurImpactApplicatifEntity indicateurImpactApplicatif1 = Instancio.create(IndicateurImpactApplicatifEntity.class);
        IndicateurImpactApplicatifEntity indicateurImpactApplicatif2 = Instancio.create(IndicateurImpactApplicatifEntity.class);
        IndicateurImpactApplicatifEntity indicateurImpactApplicatif3 = Instancio.create(IndicateurImpactApplicatifEntity.class);
        IndicateurImpactApplicatifEntity indicateurImpactApplicatif4 = Instancio.create(IndicateurImpactApplicatifEntity.class);
        IndicateurImpactApplicatifEntity indicateurImpactApplicatif5 = Instancio.create(IndicateurImpactApplicatifEntity.class);

        indicateurImpactApplicatifRepository.saveAll(Arrays.asList(indicateurImpactApplicatif1,indicateurImpactApplicatif2,indicateurImpactApplicatif3,indicateurImpactApplicatif4,indicateurImpactApplicatif5));
        assertEquals(5,indicateurImpactApplicatifRepository.findAll().size());
        restTemplate.delete(url);
        assertEquals(0,indicateurImpactApplicatifRepository.findAll().size());
    }
}