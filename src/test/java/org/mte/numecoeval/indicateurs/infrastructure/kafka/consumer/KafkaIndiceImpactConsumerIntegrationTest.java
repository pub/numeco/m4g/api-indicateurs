package org.mte.numecoeval.indicateurs.infrastructure.kafka.consumer;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactApplicatifRepository;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactEquipementPhysiqueRepository;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactEquipementVirtuelRepository;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactMessagerieRepository;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactReseauRepository;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactApplicationDTO;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactEquipementVirtuelDTO;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactMessagerieDTO;
import org.mte.numecoeval.topic.computeresult.IndicateurImpactReseauDTO;
import org.mte.numecoeval.topic.computeresult.MessageIndicateurs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles(profiles = { "test" })
@EmbeddedKafka(
        bootstrapServersProperty = "spring.kafka.bootstrap-servers",
        partitions = 1)
@Slf4j
class KafkaIndiceImpactConsumerIntegrationTest {

    @Value("${numecoeval.kafka.topic.indicateur.name}")
    String topic;
    @Autowired
    KafkaTemplate<String, MessageIndicateurs> producerIndicateurTemplate;
    @Autowired
    KafkaIndiceImpactConsumer kafkaIndiceImpactConsumer;
    @Autowired
    IndicateurImpactEquipementPhysiqueRepository indicateurImpactEquipementPhysiqueRepository;
    @Autowired
    IndicateurImpactReseauRepository indicateurImpactReseauRepository;
    @Autowired
    IndicateurImpactApplicatifRepository indicateurImpactApplicatifRepository;
    @Autowired
    IndicateurImpactMessagerieRepository indicateurImpactMessagerieRepository;
    @Autowired
    private IndicateurImpactEquipementVirtuelRepository indicateurImpactEquipementVirtuelRepository;



    @Test
    void shouldConsumeRecordAndSaveData_whenValidInput(){
        //GIVEN

        String etapeACV = "UTILISATION";
        String critere = "Changement climatique";
        LocalDateTime dateCalcul = LocalDateTime.of(2022, Month.JUNE, 4, 17, 30);

        var indicateurImpactReseauDTO= IndicateurImpactReseauDTO.builder()
                .impactUnitaire(12d)
                .unite("CO2")
                .etapeACV(etapeACV)
                .critere(critere)
                .dateCalcul(dateCalcul)
                .reference("Apple")
                .build();

        var indicateurImpactEquipementPysiqueDTO= IndicateurImpactEquipementPhysiqueDTO.builder()
                .impactUnitaire(23d)
                .unite("KW")
                .etapeACV(etapeACV)
                .critere(critere)
                .dateCalcul(dateCalcul)
                .reference("Apple")
                .build();
        var indicateurImpactEquipementVirutelDTO = IndicateurImpactEquipementVirtuelDTO.builder()
                .impactUnitaire(23d)
                .unite("KW")
                .etapeACV(etapeACV)
                .critere(critere)
                .dateCalcul(dateCalcul)
                .build();
        var indicateurImpactApplicatifDTO = IndicateurImpactApplicationDTO.builder()
                .impactUnitaire(235d)
                .unite("kgCO2eq")
                .domaine("domaineA")
                .sousDomaine("sousDomaineA")
                .dateCalcul(dateCalcul)
                .etapeACV(etapeACV)
                .critere(critere)
                .build();

        var indicateurImpactMessagerieDTO=  IndicateurImpactMessagerieDTO.builder()
                .impactMensuel(32d)
                .moisAnnee(202207)
                .critere(critere)
                .dateCalcul(dateCalcul)
                .build();

        MessageIndicateurs msg1 = new MessageIndicateurs();
            msg1.setImpactsReseaux(Collections.singletonList(indicateurImpactReseauDTO));
            msg1.setImpactsEquipementsPhysiques(Collections.emptyList());
            msg1.setImpactsEquipementsVirtuels(Collections.emptyList());
            msg1.setImpactApplications(Collections.emptyList());
            msg1.setImpactMessageries(Collections.emptyList());

        MessageIndicateurs msg2 = new MessageIndicateurs();
            msg2.setImpactsEquipementsPhysiques(Collections.singletonList(indicateurImpactEquipementPysiqueDTO));
            msg2.setImpactsReseaux(Collections.emptyList());
            msg2.setImpactApplications(Collections.emptyList());
            msg2.setImpactMessageries(Collections.emptyList());

        MessageIndicateurs msg3 = new MessageIndicateurs();
            msg3.setImpactsEquipementsPhysiques(Collections.emptyList());
            msg3.setImpactsReseaux(Collections.emptyList());
            msg3.setImpactApplications(Collections.singletonList(indicateurImpactApplicatifDTO));
            msg3.setImpactMessageries(Collections.emptyList());

        MessageIndicateurs msg4 = new MessageIndicateurs();
            msg4.setImpactsEquipementsPhysiques(Collections.emptyList());
            msg4.setImpactsReseaux(Collections.emptyList());
            msg4.setImpactApplications(Collections.emptyList());
            msg4.setImpactMessageries(Collections.singletonList(indicateurImpactMessagerieDTO));

        MessageIndicateurs msg5 = new MessageIndicateurs();
        msg5.setImpactsReseaux(Collections.emptyList());
        msg5.setImpactsEquipementsPhysiques(Collections.emptyList());
        msg5.setImpactsEquipementsVirtuels(Collections.singletonList(indicateurImpactEquipementVirutelDTO));
        msg5.setImpactApplications(Collections.emptyList());
        msg5.setImpactMessageries(Collections.emptyList());

        producerIndicateurTemplate.send(topic,msg1);
        producerIndicateurTemplate.send(topic,msg2);
        producerIndicateurTemplate.send(topic,msg3);
        producerIndicateurTemplate.send(topic,msg4);
        producerIndicateurTemplate.send(topic,msg5);
        // Message de flush du buffer
        producerIndicateurTemplate.send(topic+"-flush", new MessageIndicateurs());

        //WHEN
        await().atMost(10, TimeUnit.SECONDS)
                .until(()->( 1 == indicateurImpactEquipementPhysiqueRepository.count())&&(1 == indicateurImpactReseauRepository.count()));

        //THEN
        assertEquals(1, indicateurImpactEquipementPhysiqueRepository.count());
        assertEquals(1, indicateurImpactReseauRepository.count());
        assertEquals(1, indicateurImpactApplicatifRepository.count());
        assertEquals(1, indicateurImpactMessagerieRepository.count());
        assertEquals(1, indicateurImpactEquipementVirtuelRepository.count());

        var actualImpactReseau = indicateurImpactReseauRepository.findAll().get(0);
        var actualImpactEquipementPhysique = indicateurImpactEquipementPhysiqueRepository.findAll().get(0);
        var actualImpactApplicatif= indicateurImpactApplicatifRepository.findAll().get(0);
        var actualImpactMessagerie= indicateurImpactMessagerieRepository.findAll().get(0);
        var actualImpactEquipementVirtuel = indicateurImpactEquipementVirtuelRepository.findAll().get(0);

        assertEquals(indicateurImpactReseauDTO.getImpactUnitaire(),actualImpactReseau.getImpactUnitaire());
        assertEquals(indicateurImpactReseauDTO.getReference(),actualImpactReseau.getReference());
        assertEquals(indicateurImpactEquipementPysiqueDTO.getImpactUnitaire(),actualImpactEquipementPhysique.getImpactUnitaire());
        assertEquals(indicateurImpactEquipementPysiqueDTO.getReference(),actualImpactEquipementPhysique.getReference());
        assertEquals(indicateurImpactApplicatifDTO.getImpactUnitaire(),actualImpactApplicatif.getImpactUnitaire());
        assertEquals(indicateurImpactApplicatifDTO.getDateCalcul(),actualImpactApplicatif.getDateCalcul());
        assertEquals(indicateurImpactMessagerieDTO.getImpactMensuel(),actualImpactMessagerie.getImpactMensuel());
        assertEquals(dateCalcul,actualImpactMessagerie.getDateCalcul());
        assertEquals(indicateurImpactEquipementVirutelDTO.getImpactUnitaire(),actualImpactEquipementVirtuel.getImpactUnitaire());
    }
}