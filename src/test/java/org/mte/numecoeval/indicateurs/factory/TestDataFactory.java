package org.mte.numecoeval.indicateurs.factory;

import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactEquipementPhysique;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactReseau;

import java.time.LocalDateTime;

public class TestDataFactory {

    public static IndicateurImpactReseau indicateurImpactReseau(String reference, String critere, String etapeACV, String unite, LocalDateTime dateCalcul, Double impactUnitaire) {
        return IndicateurImpactReseau.builder()
                .reference(reference)
                .critere(critere)
                .etapeACV(etapeACV)
                .unite(unite)
                .dateCalcul(dateCalcul)
                .impactUnitaire(impactUnitaire)
                .versionCalcul("Test")
                .source("Test")
                .versionReferentiel("Test")
                .referentielsSources("Test")
                .build();
    }

    public static IndicateurImpactEquipementPhysique indicateurImpactEquipement(String reference, String typeEquipement, String critere, String etapeACV, String unite, LocalDateTime dateCalcul, Double impactUnitaire) {
        return IndicateurImpactEquipementPhysique.builder()
                .reference(reference)
                .typeEquipement(typeEquipement)
                .critere(critere)
                .etapeACV(etapeACV)
                .unite(unite)
                .dateCalcul(dateCalcul)
                .impactUnitaire(impactUnitaire)
                .versionCalcul("Test")
                .source("Test")
                .versionReferentiel("Test")
                .referentielsSources("Test")
                .build();
    }
}
