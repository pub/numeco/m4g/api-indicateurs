package org.mte.numecoeval.indicateurs.domain.port.output;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactEquipementPhysique;
import org.mte.numecoeval.indicateurs.factory.TestDataFactory;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.adapter.IndicateurImpactEquipementPhysiqueJpaAdapter;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactEquipementPhysiqueEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactEquipementPhysiqueRepository;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactEquipementMapper;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactEquipementMapperImpl;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

class IndicateurPersistencePortImpactEquipementTest {

    IndicateurPersistencePort<IndicateurImpactEquipementPhysique> serviceIndicateurImpactEquipement;

    @Mock
    IndicateurImpactEquipementPhysiqueRepository indicateurImpactEquipementPhysiqueRepository;

    IndicateurImpactEquipementMapper indicateurImpactEquipementMapper = new IndicateurImpactEquipementMapperImpl();

    @BeforeEach
    void setupAll() {
        MockitoAnnotations.openMocks(this);
        serviceIndicateurImpactEquipement = new IndicateurImpactEquipementPhysiqueJpaAdapter(indicateurImpactEquipementPhysiqueRepository, indicateurImpactEquipementMapper);
    }

    @Test
    void saveIndicateurImpactReseauTest() {
        ArgumentCaptor<IndicateurImpactEquipementPhysiqueEntity> valueCapture = ArgumentCaptor.forClass(IndicateurImpactEquipementPhysiqueEntity.class);
        var impactEquipement = TestDataFactory.indicateurImpactEquipement(
                "Apple Iphone 11",
                "Téléphone",
                "Changement Climatique",
                "UTILISATION",
                "gCO2 eq / an",
                LocalDateTime.now(),
                2.9d);

        assertDoesNotThrow( () -> serviceIndicateurImpactEquipement.save(impactEquipement) );

        Mockito.verify(indicateurImpactEquipementPhysiqueRepository, times(1)).save(valueCapture.capture());
        assertNotNull(valueCapture.getValue());

        var entitySaved = valueCapture.getValue();
        assertEquals(impactEquipement.getReference(), entitySaved.getReference());
        assertEquals(impactEquipement.getTypeEquipement(), entitySaved.getTypeEquipement());
        assertEquals(impactEquipement.getImpactUnitaire(), entitySaved.getImpactUnitaire());
        assertEquals(impactEquipement.getUnite(), entitySaved.getUnite());
        assertEquals(impactEquipement.getConsoElecMoyenne(), entitySaved.getConsoElecMoyenne());
        assertEquals(impactEquipement.getCritere(), entitySaved.getCritere());
        assertEquals(impactEquipement.getEtapeACV(), entitySaved.getEtapeACV());
        assertEquals(impactEquipement.getDateCalcul(), entitySaved.getDateCalcul());
        assertEquals(impactEquipement.getSource(), entitySaved.getSource());
        assertEquals(impactEquipement.getVersionCalcul(), entitySaved.getVersionCalcul());
        assertEquals(impactEquipement.getReferentielsSources(), entitySaved.getReferentielsSources());
        assertEquals(impactEquipement.getVersionReferentiel(), entitySaved.getVersionReferentiel());    }

    @Test
    void saveAllIndicateurImpactReseauTest() {
        ArgumentCaptor<List<IndicateurImpactEquipementPhysiqueEntity>> valueCapture = ArgumentCaptor.forClass(List.class);
        var impactEquipements = TestDataFactory.indicateurImpactEquipement(
                "Apple Iphone 11",
                "Téléphone",
                "Changement Climatique",
                "UTILISATION",
                "gCO2 eq / an",
                LocalDateTime.now(),
                2.9d);

        assertDoesNotThrow( () -> serviceIndicateurImpactEquipement.saveAll(Collections.singletonList(impactEquipements)) );

        Mockito.verify(indicateurImpactEquipementPhysiqueRepository, times(1)).saveAll(valueCapture.capture());
        assertNotNull(valueCapture.getValue());
        assertFalse(valueCapture.getValue().isEmpty());
        assertEquals(1, valueCapture.getValue().size());

        var entitySaved = valueCapture.getValue().get(0);
        assertEquals(impactEquipements.getReference(), entitySaved.getReference());
        assertEquals(impactEquipements.getTypeEquipement(), entitySaved.getTypeEquipement());
        assertEquals(impactEquipements.getImpactUnitaire(), entitySaved.getImpactUnitaire());
        assertEquals(impactEquipements.getUnite(), entitySaved.getUnite());
        assertEquals(impactEquipements.getConsoElecMoyenne(), entitySaved.getConsoElecMoyenne());
        assertEquals(impactEquipements.getCritere(), entitySaved.getCritere());
        assertEquals(impactEquipements.getEtapeACV(), entitySaved.getEtapeACV());
        assertEquals(impactEquipements.getDateCalcul(), entitySaved.getDateCalcul());
        assertEquals(impactEquipements.getSource(), entitySaved.getSource());
        assertEquals(impactEquipements.getVersionCalcul(), entitySaved.getVersionCalcul());
        assertEquals(impactEquipements.getReferentielsSources(), entitySaved.getReferentielsSources());
        assertEquals(impactEquipements.getVersionReferentiel(), entitySaved.getVersionReferentiel());
    }

    @Test
    void saveAllIndicateurImpactReseauListVideTest() {
        assertDoesNotThrow( () -> serviceIndicateurImpactEquipement.saveAll(Collections.emptyList()) );

        Mockito.verify(indicateurImpactEquipementPhysiqueRepository, times(0)).saveAll(Mockito.any());
    }
}
