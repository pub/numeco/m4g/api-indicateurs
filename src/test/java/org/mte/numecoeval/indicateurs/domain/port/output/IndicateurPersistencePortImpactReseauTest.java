package org.mte.numecoeval.indicateurs.domain.port.output;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.indicateurs.domain.model.IndicateurImpactReseau;
import org.mte.numecoeval.indicateurs.factory.TestDataFactory;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.adapter.IndicateurImpactReseauJpaAdapter;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.entity.IndicateurImpactReseauEntity;
import org.mte.numecoeval.indicateurs.infrastructure.jpa.repository.IndicateurImpactReseauRepository;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactReseauMapper;
import org.mte.numecoeval.indicateurs.infrastructure.mapper.IndicateurImpactReseauMapperImpl;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

class IndicateurPersistencePortImpactReseauTest {

    IndicateurPersistencePort<IndicateurImpactReseau> serviceIndicateurImpactReseau;

    @Mock
    IndicateurImpactReseauRepository indicateurImpactReseauRepository;

    IndicateurImpactReseauMapper indicateurImpactReseauMapper = new IndicateurImpactReseauMapperImpl();

    @BeforeEach
    void setupAll() {
        MockitoAnnotations.openMocks(this);
        serviceIndicateurImpactReseau = new IndicateurImpactReseauJpaAdapter(indicateurImpactReseauRepository, indicateurImpactReseauMapper);
    }

    @Test
    void saveIndicateurImpactReseauTest() {
        ArgumentCaptor<IndicateurImpactReseauEntity> valueCapture = ArgumentCaptor.forClass(IndicateurImpactReseauEntity.class);
        var impactReseau = TestDataFactory.indicateurImpactReseau(
                "Apple Iphone 11",
                "Changement Climatique",
                "UTILISATION",
                "gCO2 eq / an",
                LocalDateTime.now(),
                2.9d);

        assertDoesNotThrow( () -> serviceIndicateurImpactReseau.save(impactReseau) );

        Mockito.verify(indicateurImpactReseauRepository, times(1)).save(valueCapture.capture());
        assertNotNull(valueCapture.getValue());

        var entitySaved = valueCapture.getValue();
        assertEquals(impactReseau.getReference(), entitySaved.getReference());
        assertEquals(impactReseau.getCritere(), entitySaved.getCritere());
        assertEquals(impactReseau.getEtapeACV(), entitySaved.getEtapeACV());
        assertEquals(impactReseau.getDateCalcul(), entitySaved.getDateCalcul());
        assertEquals(impactReseau.getUnite(), entitySaved.getUnite());
        assertEquals(impactReseau.getSource(), entitySaved.getSource());
        assertEquals(impactReseau.getVersionCalcul(), entitySaved.getVersionCalcul());
        assertEquals(impactReseau.getReferentielsSources(), entitySaved.getReferentielsSources());
        assertEquals(impactReseau.getVersionReferentiel(), entitySaved.getVersionReferentiel());
        assertEquals(impactReseau.getImpactUnitaire(), entitySaved.getImpactUnitaire());
    }

    @Test
    void saveAllIndicateurImpactReseauTest() {
        ArgumentCaptor<List<IndicateurImpactReseauEntity>> valueCapture = ArgumentCaptor.forClass(List.class);
        var impactReseau = TestDataFactory.indicateurImpactReseau(
                "Apple Iphone 11",
                "Changement Climatique",
                "UTILISATION",
                "gCO2 eq / an",
                LocalDateTime.now(),
                2.9d);

        assertDoesNotThrow( () -> serviceIndicateurImpactReseau.saveAll(Collections.singletonList(impactReseau)) );

        Mockito.verify(indicateurImpactReseauRepository, times(1)).saveAll(valueCapture.capture());
        assertNotNull(valueCapture.getValue());
        assertFalse(valueCapture.getValue().isEmpty());
        assertEquals(1, valueCapture.getValue().size());

        var entitySaved = valueCapture.getValue().get(0);
        assertEquals(impactReseau.getReference(), entitySaved.getReference());
        assertEquals(impactReseau.getCritere(), entitySaved.getCritere());
        assertEquals(impactReseau.getEtapeACV(), entitySaved.getEtapeACV());
        assertEquals(impactReseau.getDateCalcul(), entitySaved.getDateCalcul());
        assertEquals(impactReseau.getUnite(), entitySaved.getUnite());
        assertEquals(impactReseau.getSource(), entitySaved.getSource());
        assertEquals(impactReseau.getVersionCalcul(), entitySaved.getVersionCalcul());
        assertEquals(impactReseau.getReferentielsSources(), entitySaved.getReferentielsSources());
        assertEquals(impactReseau.getVersionReferentiel(), entitySaved.getVersionReferentiel());
        assertEquals(impactReseau.getImpactUnitaire(), entitySaved.getImpactUnitaire());
    }

    @Test
    void saveAllIndicateurImpactReseauListVideTest() {
        assertDoesNotThrow( () -> serviceIndicateurImpactReseau.saveAll(Collections.emptyList()) );

        Mockito.verify(indicateurImpactReseauRepository, times(0)).saveAll(Mockito.any());
    }
}
