# api-indicateurs

Application API Indicateurs chargés de sauvegarder les indicateurs reçus via le bus de message et produit par l'API Calculs.

## Module Archivé
Ce module est archivé et fait partie de la V1 du système NumEcoEval.

Ce module a été remplacé par [api-event-indicateurs](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/api-event-indicateurs)
pour la partie persistance des indicateurs.

La partie accès par API REST aux indicateurs a été écarté en V2 car non utilisés.

Les images Docker et le code peuvent encore être consultés, mais **ce code ne sera plus maintenu**.