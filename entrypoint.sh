#!/bin/bash
echo "Vérification de la présence du fichier d'environnement"
if [ -d /run/secrets ]; then
  echo "Chargement des fichiers d'environnement"
  for secretFile in /run/secrets/*; do
    if [ -f "$secretFile" ]; then
      . $secretFile
    fi
  done
fi

echo "Lancement de l'application"
exec java -jar /app/app.jar
